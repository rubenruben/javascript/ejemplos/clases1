// Clase SeleccionFutbol.
// Creamos las clases con funciones prototipo
var SeleccionFutbol=function(datos){
   //Propiedades
   //Las iniciamos para que existan
	 this.id=0;
	 this.nombre=0;
	 this.apellidos=0;
	 this.edad=0;

       //Metodos
   	   this.setId=function(valor){
   	 	 this.id=valor || 0;
  	   }  
       this.getId=function(){
       	return this.id;
       }
   	   this.setNombre=function(valor){
   	 	 this.nombre=valor || 0;
  	   }  
       this.getNombre=function(){
       return this.nombre;
       }
    	 this.setApellidos=function(valor){
   	 	 this.apellidos=valor || 0;
  	   }  
       this.getApellidos=function(){
       return this.apellidos;
       }    
    	 this.setEdad=function(valor){
   	 	 this.edad=valor;
  	   }  
       this.getEdad=function(){
       return this.edad;
       }
  	   this.concentrase=function(){ 
  	   }
  	   this.viajar=function(){
  	   }
     
    //Constructor
    //Creamos constructores con funciones constructoras
    this.seleccionFutbol=function(v){
    		this.setId(v.id); 
    		this.setNombre(v.nombre);
    		this.setApellidos(v.apellidos);
    		this.setEdad(v.edad);	
	  }
	  this.seleccionFutbol(datos); // Escrito como en la funcion constructora

}
//Clase Futbolista
var Futbolista=function(datos){
   //Propiedades
   //Las iniciamos para que existan
	  this.dorsal=0; // this.dorsal=null
	  this.demarcacion=0;

        //Metodos
  	    this.setDorsal=function(valor){
        this.dorsal=valor || 0;
        }
        this.getDorsal=function(){
      	return this.dorsal;
        }
        this.setDemarcacion=function(valor){
        this.demarcacion=valor || 0;
  	    }
        this.getDemarcacion=function(){
      	return this.demarcacion;
        }
  	    this.JugarPartido=function(valor){
          console.log("jugando");
  	    }
  	    this.setEntrenar=function(valor){		
          return "Estoy entrenanado";
  	    }

    //constructor
	  this.futbolista=function(v){
		this.setDorsal(v.dorsal);
		this.setDemarcacion(v.demarcacion);
    this.seleccionFutbol(datos); // tengo que llamar al padre para que coja los datos,
    //coge la herencia pero no le pasa los datos
    /*TAMBIEN DEFINE LA HERENCIA
    SeleccionFutbol.call(this,datos);
    En caso de sobreescritura(dos miembros con mismo identificador) 
    no duplica lo comun y se queda con el del ascendente.
    Se utiliza si no se tienen identificadores con el mismo nombre

    */
	  }
	  this.futbolista(datos);// Escrito como en la funcion constructora
    }

//Clase Entrenador
 var Entrenador=function(datos){
 	
 	  this.idfederacion=0;

 	    this.setIdFederacion=function(valor){
 		  this.idfederacion=valor || 0;
 	    }
 	    this.getIdFederacion=function(){
 		  return this.idfederacion;
 	    } 
 	   /*this.DirigirPartido=function(valor){
 	    }
     	this.DirigirEntrenamiento=function(valor){	
 	    }*/
      //constructor
 	    this.entrenador=function(v){
 		    this.setIdFederacion(v.idfederacion);
        this.seleccionFutbol(datos);
 	    }
      this.entrenador(datos);// Escrito como en la funcion constructora
 }

 //Clase Masajista
 var Masajista=function(datos){

 	  this.titulacion=0;
 	  this.aniosexperiencia=0;

 	    this.setTitulacion=function(valor){
 		  this.titulacion=valor || 0;
 	    }
 	    this.getTitulacion=function(){
 		  return this.titulacion;
 	    }
 	    this.setAniosExperiencia=function(valor){
 		  this.aniosexperiencia=valor || 0;
 	    }
 	    this.getAniosExperiencia=function(){
 		  return this.aniosexperiencia;
 	    }
      //constructor
 	    this.masajista=function(v){
 		     this.setTitulacion(v.titulacion);
 		     this.setAniosExperiencia(v.aniosexperiencia);
         this.seleccionFutbol(datos);
    	}
      this.masajista(datos); // Escrito como en la funcion constructora

 }

 //Creamos las herencias
   Futbolista.prototype=new SeleccionFutbol({});
   Entrenador.prototype=new SeleccionFutbol({});
   Masajista.prototype=new SeleccionFutbol({});
 //Creamos objetos
 var pepe=new Futbolista({
  id:1000,
  //nombre:"Pepe",
  apellidos:"Fernandez",
  edad:22,
 	dorsal:1,
 	demarcacion:"portero",
  id:1000
 });
 pepe.nombre="Pepe"; //tambien puedo asignar el dato así
 

 var juan=new Entrenador({
  id:2000,
  nombre:"Juan",
  apellidos:"Gonzalez",
  edad:52,
  idfederacion:"cantabra"
  
 });

 var javier= new Masajista({
  	titulacion:"masajista",
  	aniosexperiencia:5,
    id:3000
 });
  console.log(pepe.getId());
  console.log(pepe.getNombre());
  console.log(pepe.getApellidos());
  console.log(pepe.getEdad());
  console.log(pepe.getDorsal());
  console.log(pepe.getDemarcacion());
  
  console.log(juan.getId());
  console.log(juan.getNombre());
  console.log(juan.getApellidos());
  console.log(juan.getEdad());
  console.log(juan.getIdFederacion());

  console.log(javier.getId());
  console.log(javier.getTitulacion());
  console.log(javier.getAniosExperiencia());
  console.log(javier.getId());

  console.log(juan);//Muestra todo del objeto juan
  
  /*
    En caso de sobreescritura y uso de descendente.prototype=new ascendente({})
    console.log(descendente.nombre)// muestra nombre del descendente
    console.log(descendente._proto_.nombre) // muestra nombre del ascendente
    Tantos _protos como grado de descendencia.
  */
var resultado=[];
  function listaTodasLasPropiedades(o){
   var juan;
   //var resultado = [];

   for(juan = o; juan !== null; juan = Object.getPrototypeOf(juan)){
      resultado = resultado.concat(Object.getOwnPropertyNames(juan)) + "\n";
   }   

   return resultado; 

}
listaTodasLasPropiedades(juan);
console.log(resultado);

