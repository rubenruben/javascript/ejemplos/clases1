//Clase persona
var Persona=function(datos){
	this.nombre=0;
	this.direccion=0;
	this.correoElectronico=0;

		this.setNombre=function(valor){
			this.nombre=valor || 0;
		}
		this.getNombre=function(){
			return this.nombre;
		}
		this.setDireccion=function(valor){
			this.direccion=valor || 0;
		}
		this.getDireccion=function(){
			return this.direccion;
		}
		this.setCorreoElectronico=function(valor){
			this.correoElectronico=valor || 0;
		}
		this.getCorreoElectronico=function(valor){
			return this.correoElectronico;
		}
		
        this.crear=function(){

        }
        this.borrar=function(){
        	
        }
        this.enviarMensaje=function(){
        	
        }
        
    //constructor
    this.persona=function(v){
    	this.setNombre(v.nombre);
    	this.setDireccion(v.direccion);
    	this.setCorreoElectronico(v.correoElectronico);
    }
    this.persona(datos);
}

//Clase cliente
var Cliente=function(datos){
	this.numeroCliente=0;
	this.fechaAlta=0;

		this.setNumeroCliente=function(valor){
			this.numeroCliente=valor || 0;
		}
		this.getNumeroCliente=function(){
			return this.numeroCliente;
		}
		this.setFechaAlta=function(valor){
			this.fechaAlta=valor || 0;
		}
		this.getFechaAlta=function(){
			return this.fechaAlta;
		}
		this.verFechaAlta=function(){

		}
	//Constructor
	this.cliente=function(v){
		this.setNumeroCliente(v.numeroCliente);
		this.setFechaAlta(v.fechaAlta);
		this.persona(datos);
	}
	
	this.cliente(datos);	

}
//Clase Usuario
var Usuario=function(datos){
	this.codigoUsuario=0;

		this.setCodigoUsuario=function(valor){
			this.codigoUsuario=valor || 0;
		}
		this.getCodigoUsuario=function(){
			return this.codigoUsuario;
		}
		this.autorizar=function(){

		}
		this.crear=function(){

		}
	//Constructor
	this.usuario=function(v){
		this.setCodigoUsuario(v.codigoUsuario);
		this.persona(datos);
	}

	this.usuario(datos);	
}

//Creamos las herencias
Cliente.prototype=new Persona({});
Usuario.prototype=new Persona({});

//Creamos objetos

var pepe=new Cliente({
	nombre:"Pepe",
	direccion:"Santander",
	correoElectronico:"pp@yahoo.es",
	numeroCliente:1,
	fechaAlta:"1/10/2012"
});

var juan=new Usuario({
	nombre:"Juan",
	direccion:"Santander",
	correoElectronico:"juan@yahoo.es",
	codigoUsuario:1
});

  
  console.log(pepe.getNombre());
  console.log(pepe.getDireccion());
  console.log(pepe.getCorreoElectronico());
  console.log(pepe.getNumeroCliente());
  console.log(pepe.getFechaAlta());

  console.log(juan.getNombre());
  console.log(juan.getDireccion());
  console.log(juan.getCorreoElectronico());
  console.log(juan.getCodigoUsuario());
 